# Calories Management Solution#

## Quick summary

This solution provides a user friendly interface through which users can note down and manage their meals. 
Those meals can later be used to track the calories input given time ranges.
Manager users are able to manage meals for other users.
The interface also provides an interface through which an admin user can manage other users.
All operations are delegated to an API application.


## Setup

The solution is comprised of 2 projects:

- CaloriesMgmtAPI: contains the rest services and data acess to perform the data CRUD and queries

Tests are available under the src/test folder. Both Unit Test and Integration Tests are available.
Maven is configured so only unit tests will be ran when the package is built (mvn clean install).
In  order to run integration tests, one has to use the "integration" profile when running the test target (mvn test -P integration)

- CaloriesMgmt: contains the web application through which users can perform the user and meal managements

One of the requirements was the user authentication was done through a Rest API - so that's what is being done. Besides that, the user roles 
are also fetched once the user is authenticated, so specific parts of the web application can be shown/hidden depending on the user role.


The projects are setup for development, so the schema is recreated and test data popupated everytime the application is deployed. When the application is undeployed, the schema is destroyed.
In order to setup for production, one has to change the persistence to "update" in application.properties, and to disable the SQL script executed automatically (application.properties and src/main/resources/data-postgresql.sql)