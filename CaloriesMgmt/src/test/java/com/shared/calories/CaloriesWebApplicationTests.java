package com.shared.calories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.shared.calories.CaloriesWebApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = CaloriesWebApplication.class)
@WebAppConfiguration
public class CaloriesWebApplicationTests {

	@Test
	public void contextLoads() {
	}

}
