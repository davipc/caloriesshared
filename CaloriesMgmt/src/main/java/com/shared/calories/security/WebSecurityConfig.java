package com.shared.calories.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.shared.calories.constants.WebResources;
import com.shared.calories.entity.RoleType;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    private RESTBasedAuthenticationProvider authProvider;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        log.debug("Setting up security...");

    	http.authorizeRequests()
    			.antMatchers(WebResources.SIGNUP_PAGE).permitAll()
    			.antMatchers(WebResources.LANDING_PAGE).authenticated()
                .antMatchers(WebResources.MEAL_MANAGE_PAGE, WebResources.USER_MANAGE_PAGE, WebResources.USER_SESSION_UPDATE_CHECK).access("hasRole('ROLE_" + RoleType.DEFAULT + "') or hasRole('ROLE_" + RoleType.MANAGER + "') or hasRole('ROLE_" + RoleType.ADMIN + "')")
            .and()
	            // ATTENTION: For the form login to work, the attribute names must be EXACTLY these: username=<username>, password=<password> AND Submit=Login
	            // IF YOU WANT THE 2 FIRST TO BE DIFFERENT, YOU HAVE TO SET IT LIKE THIS:
	            //.formLogin().loginPage("/login.jsp").usernameParameter("<other name>").passwordParameter("<other name>");
            	// on a side note, the defaultSuccessUrl is forcing the user to always go to the index page
	            .formLogin().loginPage(WebResources.LOGIN_PAGE).defaultSuccessUrl(WebResources.LANDING_PAGE, true).loginProcessingUrl("/perform_login")
            .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl(WebResources.LOGIN_PAGE)
            ;//.and().addFilterAfter(new CsrfCustomFilter(), CsrfFilter.class);
        
		log.debug("Finished setting up security");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        log.debug("Configuring authentication...");

        auth.authenticationProvider(authProvider);
        
        log.debug("Finished configuring authentication");
    }
}
