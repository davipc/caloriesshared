package com.shared.calories.security;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.shared.calories.constants.RestPaths;
import com.shared.calories.entity.User;


import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class RESTBasedAuthenticationProvider implements AuthenticationProvider {

	@Value("${com.shared.rest.baseURI}")
	private String endpointBase;
	
    @Override
    public Authentication authenticate(Authentication authentication)
	throws AuthenticationException {
        Authentication result = null; 
    	String name = authentication.getName();

        log.debug("Authenticating user " + name);
        // You can get the password here
        String password = authentication.getCredentials().toString();

        // call authentication method
        User authenticatedUser = callCustomAuthenticationMethod(name, password);

        // if authenticated (and thus returned a user), create the return object with the credentials + roles  
        if (authenticatedUser != null) {
        	
        	List<? extends GrantedAuthority> authorities = authenticatedUser.getRoles();
        	
        	// after authentication succeeds, get bearer token
        	String token = getRestToken(name, password);
        	authenticatedUser.setOAuthToken(token);
        	
        	result = new UsernamePasswordAuthenticationToken(authenticatedUser, password, authorities);
        }

        log.debug("Finished authenticating user " + name);
        
        return result;
    }

	@Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    /**
     * Calls the authentication REST API
     * @param username
     * @param password
     * @return
     */
    private User callCustomAuthenticationMethod(String username, String password) {
    	User user = null;
    	
    	User loginUser = new User();
    	loginUser.setLogin(username);
    	loginUser.setPassword(password);
    	
    	RestTemplate restTemplate = new RestTemplate();
    	
    	URI endpoint = null;
    	
    	try {
    		
    		log.debug("Using configured endpoint base " + endpointBase);
    		
    		endpoint = new URI(endpointBase + RestPaths.AUTH);
		} catch (URISyntaxException e) {
			// nothing to do if the URL is wrong
			log.error("Bad Auth service URI", e);
			return null;
		}
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.APPLICATION_JSON);    	
    	headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
    	
    	HttpEntity<User> entity = new HttpEntity<User>(loginUser,headers);
    	
    	try {
	    	ResponseEntity<User> response = restTemplate.postForEntity(endpoint, entity, User.class);
	    	
	    	user = response.getBody();
	    	
	    	if (response.getStatusCode().value() != HttpServletResponse.SC_OK) {
	    		log.warn("Login attempt for user " + username + " failed with status " + response.getStatusCode());
	    	}
    	} catch (HttpClientErrorException e) {
    		log.warn("Error authenticating user " + username + ": " + e.getMessage());
    	}
    	return user;
    }
    
    private String getRestToken(String username, String password) {
		String token = null;
		
    	RestTemplate restTemplate = new RestTemplate();
    	
    	URI endpoint = null;
    	
    	try {
    		
    		log.debug("Using configured oauth endpoint " + endpointBase + RestPaths.OAUTH_TOKEN);
    		
    		endpoint = new URI(endpointBase + RestPaths.OAUTH_TOKEN);
		} catch (URISyntaxException e) {
			// nothing to do if the URL is wrong
			log.error("Bad oAuth service URI", e);
			return null;
		}
    	
    	MultiValueMap<String, String> mvm = new LinkedMultiValueMap<String, String>();
    	mvm.add("username", username);
    	mvm.add("password", password);
    	mvm.add("grant_type", "password");
    	mvm.add("scope", "read write");
    	mvm.add("client_id", "clientapp");
    	mvm.add("client_secret", "123456");
    	
    	HttpHeaders headers = new HttpHeaders();
    	headers.setAccept(Arrays.asList(new MediaType[]{MediaType.APPLICATION_JSON}));
    	headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    	
    	byte[] encodedAuth = Base64.getEncoder().encode("clientapp:123456".getBytes(Charset.forName("US-ASCII")) );
    	
    	headers.set("Authorization", "Basic " + new String(encodedAuth));
    	
    	HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(mvm, headers);
    	
    	try {
	    	ResponseEntity<String> response = restTemplate.postForEntity(endpoint, entity, String.class);
	    	
	    	if (response.getStatusCode().value() != HttpServletResponse.SC_OK) {
	    		log.warn("Rest API login attempt for user " + username + " failed with status " + response.getStatusCode());
	    	} else {
		    	String responseStr = response.getBody();
		    	log.info("Response: " + responseStr);
		    	
		    	JSONObject jsonObj = new JSONObject(responseStr);
		    	
		    	token = jsonObj.getString("access_token");
	    	}
	    	
    	} catch (HttpClientErrorException e) {
    		log.warn("Error authorizing user " + username + " on Rest Api: " + e.getMessage());
    	}
		
		return token;
	}

    
}