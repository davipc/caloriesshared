package com.shared.calories.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * The persistent class for the app_user database table.
 * 
 */

@Data
// need to exclude password since it is not returned from JSON serializations (REST API reads, for instance)
@EqualsAndHashCode(exclude="password", callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
// need to exclude meals list to break cycle
@ToString(exclude="meals")
@Builder(toBuilder = true)

public class User extends BaseEntity implements Serializable {

	private static final long serialVersionUID = -359261847815777816L;
	
	private Integer id;
	private String login;
	private String password;
	private String name;
	private Gender gender;
	private Integer dailyCalories;
	private List<Role> roles;
	private List<Meal> meals;
	private Timestamp creationDt;
	
	private String oAuthToken;

	
	public User(User otherUser) {
		this.id = otherUser.id;
		this.login = otherUser.login;
		this.password = otherUser.password;
		this.name = otherUser.name;
		this.gender = otherUser.gender;
		this.dailyCalories = otherUser.dailyCalories;
		
		if (otherUser.roles != null) {
			this.roles = new ArrayList<Role>(otherUser.roles);
		}
//		if (otherUser.meals != null && otherUser.meals.size() > 0) {
//			this.meals = new ArrayList<Meal>(otherUser.meals);
//		}
		
		this.oAuthToken = otherUser.oAuthToken;
	}

	public boolean hasRole(RoleType roleToCheck) {
		boolean result = false;
		
		for (Role role: roles) {
			if (role.getName().equals(roleToCheck)) {
				result = true;
				break;
			}
		}
			
		return result;
	}
	
	public String getJSON() {
		StringBuilder builder = new StringBuilder("{");
		builder.append("\"id\":").append(id).append(",");
		builder.append("\"login\":").append(quoteObject(login)).append(",");
		builder.append("\"password\":").append(quoteObject(password)).append(",");
		builder.append("\"name\":").append(quoteObject(name)).append(",");
		builder.append("\"gender\":").append(quoteObject(gender)).append(",");
		builder.append("\"dailyCalories\":").append(dailyCalories).append(",");
		builder.append("\"roles\":");
		
		if (roles == null) {
			builder.append("null, ");
		} else {
			builder.append("[");
			if (roles.size() > 0) {
				builder.append(roles.get(0).getJSON());
				for (int i = 1; i < roles.size(); i++) {
					builder.append(", ").append(roles.get(i).getJSON());
				}
			}
			builder.append("], ");
		}
		builder.append("\"creationDt\":").append(creationDt != null ? creationDt.getTime() : null).append(",");
		builder.append("\"oAuthToken\":").append(quoteObject(oAuthToken));
		builder.append("}");
		
		return builder.toString();
	}
	
	
	/**
	 * Validates the object, returning a list of errors if any exist, or null otherwise
	 * @return
	 */
	public String validate() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(validateString(login, true, 12, "login"));
		// no need to worry about password field size - hash function will always return 64 chars
		sb.append(validateString(password, false, Integer.MAX_VALUE, "password"));
		sb.append(validateString(name, true, 80, "name"));
		sb.append(validateForNull(gender, true, "gender"));
		sb.append(validateForNull(dailyCalories, true, "daily calories"));
		sb.append(validateForNull(creationDt, true, "creation date"));
		
		String result = sb.toString();
		if (result.trim().equals(""))
			result = null;
		
		return result;
	}
}