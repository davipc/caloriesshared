package com.shared.calories.entity;

import org.springframework.security.core.GrantedAuthority;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the role database table.
 * 
 */

@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)

public class Role extends BaseEntity implements GrantedAuthority {

	private static final long serialVersionUID = 1582621316792956215L;

	private Integer id;
	private RoleType name;

	@Override
	public String getAuthority() {
		return "ROLE_" + name.name();
//		return name.name();
	}	
	
	public String getJSON() {
		StringBuilder builder = new StringBuilder("{");
		builder.append("\"id\":").append(id).append(",");
		builder.append("\"name\":").append(quoteObject(name));
		builder.append("}");
		
		return builder.toString();
	}
}