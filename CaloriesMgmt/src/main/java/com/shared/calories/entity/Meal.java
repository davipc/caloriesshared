package com.shared.calories.entity;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the meal database table.
 * 
 * Notes on the meal date and time choice:
 * 
 * 
 * We will not store in a timestamp with TZ because:  
 *   - there are no queries related to timezone (there's no usable report considering meal date/time TZs)
 *   - if a user travels from one location to another, he will still want to always store the meal time information in the local timezone
 *     
 * We will not store in a timestamp without TZ because:  
 *   - JPA queries in this format are platform dependent, and more complex
 *   
 * So we will store date and time in distinct columns, so: 
 * 	- we will always store absolute date and time values (DST and TZ independent)
 *  - queries will be simpler
 *  
 */


// generate getters, setters, constructors, toString, equals, hash  
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)

public class Meal extends BaseEntity {

	private Integer id;
    private User user;
	private Date mealDate;
	private Date mealTime;
	private String description;
	private Integer calories;
}