package com.shared.calories;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaloriesWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaloriesWebApplication.class, args);
	}
}
