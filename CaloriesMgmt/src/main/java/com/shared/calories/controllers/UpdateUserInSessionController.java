package com.shared.calories.controllers;

import org.json.JSONObject;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shared.calories.entity.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class UpdateUserInSessionController {
	
	@RequestMapping(value="/updateUserInSession", method=RequestMethod.POST)
	public @ResponseBody String updateUserInSession(@RequestBody User changedUser, @AuthenticationPrincipal User activeUser) {
		JSONObject result = new JSONObject();
		
		log.info("Executing updateUserInSession method...");
		
		if (changedUser == null) {
			result.append("result", "Input user was null");
		} else if (activeUser == null) {
			result.append("result", "No user in session");
		} else if (changedUser.getId() == null || !changedUser.getId().equals(activeUser.getId())) {
			result.append("result", "Input user is not the same as user in session");
		} else {
			// skip id (not safe)
			activeUser.setLogin(changedUser.getLogin());
			activeUser.setName(changedUser.getName());
			// skip password  (not safe)
			activeUser.setGender(changedUser.getGender());
			// skip roles  (not safe)
			// skip meals  (not safe)
			// skip creationDt (it never changes)
			activeUser.setDailyCalories(changedUser.getDailyCalories());
			
			result.append("result", "User details successfully updated");
		}
		
		return result.toString();
	}

}
