package com.shared.calories.constants;

public class WebResources {
	public static final String LOGIN_PAGE = "/login";
	public static final String SIGNUP_PAGE = "/userNew";
	public static final String LANDING_PAGE = "/index";
	public static final String MEAL_MANAGE_PAGE = "/calendar";
	public static final String USER_MANAGE_PAGE = "/userEdit";
	
	public static final String USER_SESSION_UPDATE_CHECK = "/updateUserInSession";
	
}
