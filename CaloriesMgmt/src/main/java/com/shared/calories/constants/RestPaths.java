package com.shared.calories.constants;

public class RestPaths {
	public static String REST_BASE_URI = "/api/v2";
	
	public static final String AUTH = REST_BASE_URI + "/auth"; 
	public static final String ROLES = REST_BASE_URI + "/roles";
	public static final String USERS = REST_BASE_URI + "/users";
	public static final String MEALS = REST_BASE_URI + "/meals";
	public static final String OAUTH_TOKEN = "/oauth/token";
}
