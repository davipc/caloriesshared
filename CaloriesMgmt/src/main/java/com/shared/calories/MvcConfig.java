package com.shared.calories;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

	@Override
    public void addViewControllers(ViewControllerRegistry registry) {
		log.debug("Setting up view controllers...");
		
		// controller is the address to be entered in the browser, view is the page file name 
		registry.addViewController("/").setViewName("redirect:/index");
        registry.addViewController("index").setViewName("index");
        registry.addViewController("login").setViewName("login");
        registry.addViewController("calendar").setViewName("calendarCalories");
        registry.addViewController("userSignup").setViewName("userNew");
        registry.addViewController("userEdit").setViewName("userEdit");
        
        // used when a JSP was used to update the user in session - now we are using a controller instead
        //registry.addViewController("updateUserInSession").setViewName("updateUserInSession");
        
		log.debug("Finished setting up view controllers");
    }
	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		log.debug("Enabling serlvet handling...");
		
        configurer.enable();

        log.debug("Finished enabling serlvet handling");
    }
	
    @Bean  
    public UrlBasedViewResolver setupViewResolver() { 
    	log.debug("Setting up view resolver...");
    	
        UrlBasedViewResolver resolver = new UrlBasedViewResolver();  
        resolver.setPrefix("/WEB-INF/pages/");  
        resolver.setSuffix(".jsp");  
        resolver.setViewClass(JstlView.class);  
        return resolver;  
    }
}
