<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  

<%@ page import="com.shared.calories.constants.RestPaths" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>
<meta charset='utf-8' />

<title>Calories Management</title>

<link href='lib/cupertino/jquery-ui.min.css' rel='stylesheet' />
<link href='lib/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='lib/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="lib/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" >
<!-- Custom CSS -->
<link href="css/simple-sidebar.css" rel="stylesheet">

<script src='lib/moment.min.js'></script>
<script src='lib/jquery.min.js'></script>
<script src='lib/fullcalendar/fullcalendar.js'></script>
<script src="lib/angular/angular.min.js"></script>
<script src="js/controllers.js"></script>
<script src="lib/bootstrap/bootstrap.min.js"></script>
<style>
   #calendar {
               max-width: 900px;
               margin: 0 auto;
       }
       
	/* puts an asterisk on required fields. Requires the div to be tagged as required (through its class), 
	   and the label to be tagged as the control-label (also through its class) */
	.form-group.required .control-label:after {
  		content:"*";
  		color:red;
	}       
</style>
</head>
<body ng-app="MyApp">

   <div id="wrapper">

        <%@ include file="Sidebar.jsp" %>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-10">
                        <h1>Meals Calendar</h1>
                        
                		<div ng-controller="MealsCtrl">
                		
	                		<sec:authorize access="hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')">
	                			<div ng-init="init(<sec:authentication property="principal.JSON" />, true, '<spring:eval expression="@environment.getProperty('com.shared.rest.baseURI')"/><%=RestPaths.REST_BASE_URI %>')"></div>
	                		</sec:authorize>
	                		<sec:authorize access="!(hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN'))">
	                			<div ng-init="init(<sec:authentication property="principal.JSON" />, false, '<spring:eval expression="@environment.getProperty('com.shared.rest.baseURI')"/><%=RestPaths.REST_BASE_URI %>')"></div>
	                		</sec:authorize>
                		
		                    <div class="col-lg-12">
	                		
		                		<div class="col-lg-2">
			                        <br/><br/><br/> 
			                		<sec:authorize access="hasRole('ROLE_MANAGER') or hasRole('ROLE_ADMIN')">
			                		<div class="form-group">
			                		<label for="userSelect"> User: &nbsp;&nbsp;</label>
			               			<select ng-change="userChanged()"
			               			        ng-model="selectedUser" id="userSelect"
									        ng-options="user as user.login for user in users | orderBy:'login'"></select>
									</div>
									</sec:authorize>
										
								    <div>
									    <div class="form-group">
											<label for="fromDateFilter"> From date: </label>
											<input ng-model="filterFromDate" class="form-control" id="fromDateFilter" type="date" required/>
									    </div>
									    <div class="form-group">
											<label for="toDateFilter"> To date: </label>
											<input ng-model="filterToDate" class="form-control" id="toDateFilter" type="date" required/>
									    </div>
									</div>
								    <div>
									    <div class="form-group">
									    	<label for="fromTimeFilter"> From Time: </label>
									     	<input ng-model="filterFromTime" type="time" class="form-control" id="fromTimeFilter" required/>
									    </div>
									    <div class="form-group">
									    	<label for="toTimeFilter"> To Time: </label>
									     	<input ng-model="filterToTime" type="time" class="form-control" id="toTimeFilter" required/>
									    </div>
									</div>
									<button ng-click="filterMealsClicked()" class="btn btn-success">Filter Meals</button>
									<button ng-click="resetClicked()" class="btn btn-default">Reset</button>
									
									
									<br/>
									<br/> 
									<br/> 
									<br/> 
									<br/> 
									<br/> 
									<br/> 
									<br/>  
									<br/>  
									<br/>  
									<br/>
									<br/>  
									<b>Max Daily Calories:</b> {{ chosenUser.dailyCalories | number: 0 }}<br/><br/>
									<b>Average per Day:</b> {{ dailyAvgCalories | number: 0 }}
								</div>
								
		                		<div class="col-lg-10">
								
								  	<div id="calendarModal" class="modal fade">
										<div class="modal-dialog">
										    <div class="modal-content">
										        <div class="modal-header">
										            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
										            <h4 id="modalTitle" class="modal-title"></h4>
										        </div>
										        <div id="modalBody" class="modal-body"> 
												   <form name="form" role="form">
												      <div class="form-group required" ng-class="{'has-error': form.mealDescription.$dirty && form.mealDescription.$invalid}">
												        <label for="description" class="control-label">Description</label>
												        <input ng-model="meal.id" type="hidden" id="mealId">
												        <input ng-model="meal.user.id" type="hidden" id="mealUserId">
												        <input ng-model="meal.description" type="text" class="form-control" id="mealDescription" name="mealDescription" placeholder="Enter description" required />
															<span style="color:red" ng-show="form.mealDescription.$dirty && form.mealDescription.$invalid">
															  <small ng-show="form.mealDescription.$error.required">Description is required.</small>
															</span>															
												      </div>
												      <div class="form-group required" ng-class="{'has-error': form.calories.$dirty && form.calories.$invalid}">
												        <label for="calories" class="control-label">Calories</label>
												        <input ng-model="meal.calories" type="number" class="form-control" id="calories" name="calories" placeholder="Enter Calories" required/>
															<span style="color:red" ng-show="form.calories.$dirty && form.calories.$invalid">
															  <small ng-show="form.calories.$error.required">Calories is required.</small>
															</span>															
												      </div>
												      <div class="form-group required" ng-class="{'has-error': form.mealDate.$dirty && form.mealDate.$invalid}">
												        <label for="mealDate" class="control-label">Date</label>
														<input ng-model="meal.mealDate" class="form-control" id="mealDate" name="mealDate" type="date" required/>
															<span style="color:red" ng-show="form.mealDate.$dirty && form.mealDate.$invalid">
															  <small ng-show="form.mealDate.$error.required">Date is required.</small>
															</span>															
												      </div>
												      <div class="form-group required" ng-class="{'has-error': form.mealTime.$dirty && form.mealTime.$invalid}">
												        <label for="mealTime" class="control-label">Time</label>
												        <input ng-model="meal.mealTime" type="time" class="form-control" id="mealTime" name="mealTime" required/>
															<span style="color:red" ng-show="form.mealTime.$dirty && form.mealTime.$invalid">
															  <small ng-show="form.mealTime.$error.required">Time is required.</small>
															</span>															
												      </div>
												    </form>		        
										        </div>
										        <div class="modal-footer">
										            <div ng-show="!meal.id">
											            <button ng-click="newMeal(meal)" class="btn btn-success" ng-disabled="form.$invalid">New Meal</button>
											        </div>
											        <div ng-show="meal.id">
											        	<button ng-click="deleteMeal(meal)" class="btn btn-default">Remove</button>
											            <button ng-click="updateMeal(meal)" class="btn btn-success" ng-disabled="form.$invalid">Edit Meal</button>
											        </div>
											        <br/>
											        <div id="message"></div>
											    </div>
										    </div>
										</div>
									</div>
			
			  						<div id='calendar'></div>    
						  		</div>
                    		</div>
                    	</div> <!-- div controller -->
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

</body>

</html>
