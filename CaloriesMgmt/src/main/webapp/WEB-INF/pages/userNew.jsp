<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>  

<%@ page import="com.shared.calories.constants.RestPaths" %>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html lang="en">
<head>

<title>Calories Management</title>

<link href='lib/cupertino/jquery-ui.min.css' rel='stylesheet' />
<link href='lib/fullcalendar/fullcalendar.css' rel='stylesheet' />
<link href='lib/fullcalendar/fullcalendar.print.css' rel='stylesheet' media='print' />
<link href="lib/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css" >
<link href="css/simple-sidebar.css" rel="stylesheet">

<script src='lib/moment.min.js'></script>
<script src='lib/jquery.min.js'></script>
<script src='lib/fullcalendar/fullcalendar.js'></script>
<script src="lib/angular/angular.min.js"></script>
<script src="js/controller_user.js"></script>
<script src="lib/bootstrap/bootstrap.min.js"></script>
<style>
   #calendar {
               max-width: 900px;
               margin: 0 auto;
       }
       
	/* puts an asterisk on required fields. Requires the div to be tagged as required (through its class), 
	   and the label to be tagged as the control-label (also through its class) */
	.form-group.required .control-label:after {
  		content:"*";
  		color:red;
	}

	/* make labels not bold */	
	label {
	    font-weight: normal !important;
	}
	    
</style>
</head>
<body ng-app="UserApp">

   <div id="wrapper">

        <%@ include file="SidebarUserNew.jsp" %>

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-11">
                        <h1>Create your new user</h1>
                        	<br/><br/>
							<div ng-controller="NewUsersCtrl">
							                		
	                			<div ng-init="init('<spring:eval expression="@environment.getProperty('com.shared.rest.baseURI')"/><%=RestPaths.REST_BASE_URI %>')"></div>

			                    <div class="col-lg-12">
		                		
			                		<div class="col-lg-11">
	
		                		    <div class="panel panel-info">
		                        	<div class="panel-heading"> User Details</div>
			                        <div class="panel-body">
			                        <form name="userForm">
				                        <p> In this screen you can define the user information and password.</p>
				                        
				                        <br>
		
										<div class="panel panel-info">
				                       		<div class="panel-heading"> User Info </div>
			                        		<div class="panel-body">
					                       		<div class="table-responsive">
													<input ng-model="chosenUser.id" type='hidden' id='userId'>
													<TABLE id="UserDetailsTable" class="table table-bordered" >
														<thead>
															<TR>
																<TH style="width: 25%">Name</TH>
																<TH style="width: 75%">Value</TH>
															</TR>
														</thead>
														<tbody>
															
															<!-- Best way to add asterisk on mandatory fields would be by setting a specific CSS block and changing structure and having 
																required attributes inputs be in a div with its label -->
															<!-- http://stackoverflow.com/questions/23141854/adding-asterisk-to-required-fields-in-bootstrap-3 -->
															
															<tr class="form-group required" ng-class="{'has-error': userForm.login.$dirty && userForm.login.$invalid}">
															<td align="right"><label for="login" class="control-label">Login</label></td><td><input type="text" ng-model="chosenUser.login" name="login" id="login" class="form-control" ng-minlength="4" ng-maxlength="12" required>
																<span style="color:red" ng-show="userForm.login.$dirty && userForm.login.$invalid">
																  <small ng-show="userForm.login.$error.required">Login is required.</small>
																  <small ng-show="userForm.login.$error.minlength">Login needs at least 4 characters.</small>
																  <small ng-show="userForm.login.$error.maxlength">Login can have up to 12 characters.</small>
																</span>															
															</td></tr>
															<tr class="form-group required" ng-class="{'has-error': userForm.password.$dirty && userForm.password.$invalid}">
															<td align="right"><label for="password" class="control-label">Password</label></td><td><input type="password" ng-model="chosenUser.password" name="password" id="password" class="form-control" required>
																<span style="color:red" ng-show="userForm.password.$dirty && userForm.password.$invalid">
																  <small ng-show="userForm.password.$error.required">Password is required.</small>
																</span>															
															</td></tr>
															<tr class="form-group required" ng-class="{'has-error': userForm.name.$dirty && userForm.name.$invalid}">
															<td align="right"><label for="name" class="control-label">Name</label></td><td><input type="text" ng-model="chosenUser.name" name="name" id="name" class="form-control" required>
																<span style="color:red" ng-show="userForm.name.$dirty && userForm.name.$invalid">
																  <small ng-show="userForm.name.$error.required">Name is required.</small>
																</span>															
															</td></tr>
															<tr class="form-group required" ng-class="{'has-error': userForm.gender.$dirty && userForm.gender.$invalid}">
															<td align="right"><label for="gender" class="control-label">Gender</label></td><td><select id="gender" name="gender" ng-model="chosenUser.gender" class="form-control" required><option value="M">Male</option><option value="F">Female</option></select>
																<span style="color:red" ng-show="userForm.gender.$invalid">
																  <small ng-show="userForm.gender.$error.required">Gender is required.</small>
																</span>															
															</td></tr>
															<tr class="form-group required" ng-class="{'has-error': userForm.dailyCalories.$dirty && userForm.dailyCalories.$invalid}">
															<td align="right"><label for="dailyCalories" class="control-label">Daily Calories</label></td><td><input type="number" name="dailyCalories" id="dailyCalories" ng-model="chosenUser.dailyCalories" class="form-control" required>
																<span style="color:red" ng-show="userForm.dailyCalories.$dirty && userForm.dailyCalories.$invalid">
																  <small ng-show="userForm.dailyCalories.$error.required">Daily Calories is required.</small>
																</span>															
															</td></tr>
															<tr><td align="right">Created on</td><td><input type="datetime" id="creationDt" ng-model="chosenUser.creationDt" class="form-control" disabled></td></tr>
														</tbody> 
													</TABLE>
												</div>
											</div>
										</div>
																	
										<button class="btn btn-success" ng-click="createUser(chosenUser)" ng-show="!chosenUser || !chosenUser.id" ng-disabled="userForm.$invalid">Create User</button>
										<br/> <br/>
										<div id="message"></div>										
									</form>
		
									</div>
								</div>
							
							</div> <!-- User details  -->
	                		<div class="col-lg-1">
							</div>
							
							</div>
						</div>
						</div>	
						
  
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

</body>
</html>
