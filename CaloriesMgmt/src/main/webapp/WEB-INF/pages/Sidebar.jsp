

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a href="index">
                        Meals Manager
                    </a>
                </li>
                <!--  no need to secure this as it will be available to any authenticated users -->
                <li>
                    <a href="calendar">Meals Calendar</a>
                </li>
                <li>
                    <a href="userEdit">User Management</a>
                </li>
                <li>
                <br/>
                </li>
                <li>
	                <a href="logout">Logout &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(<sec:authentication property="principal.login" />)</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
