package com.shared.calories.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.shared.calories.Application;
import com.shared.calories.constants.RestPaths;
import com.shared.calories.entity.Role;
import com.shared.calories.entity.RoleType;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)

//USE OF DBUNIT IS CONFLICTING WITH TEST DATA (NEED TO REPLACE TEST DATA)
//@DatabaseSetup(ITAuthService.DATASET)
//@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { ITAuthService.DATASET })
//@DirtiesContext
//@TestExecutionListeners({ 
//		DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
//		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class 
//})
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ITRoleService {

	protected static final String DATASET = "classpath:datasets/all-entries.xml";
	
	@Value("${local.server.port}")
	private int serverPort;
	
	@Value("${server.context-path}")
	private String context;
	
	@Before
	public void setUp() {
	    RestAssured.port = serverPort;
	    RestAssured.basePath = context;
	}
	
	@Test
	public void testGetAllRoles() {
		
		Role[] roles = 
			given()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
			.expect()
				.statusCode(HttpStatus.OK.value())	
			.when()
				.get(RestPaths.ROLES)
				.as(Role[].class);
		
		assertThat(roles).hasSize(3).extracting("name").containsOnly(RoleType.DEFAULT, RoleType.MANAGER, RoleType.ADMIN);
	}
}
