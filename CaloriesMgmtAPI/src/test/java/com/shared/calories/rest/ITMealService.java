package com.shared.calories.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.config.ObjectMapperConfig;
import com.jayway.restassured.config.RestAssuredConfig;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.mapper.factory.Jackson2ObjectMapperFactory;
import com.jayway.restassured.path.json.JsonPath;
import com.shared.calories.Application;
import com.shared.calories.CustomDateDeserializer;
import com.shared.calories.TestData;
import com.shared.calories.constants.RestPaths;
import com.shared.calories.entity.Meal;
import com.shared.calories.repository.MealRepository;

/**
 * WARNING: cannot do DB rollbacks after each test - that is because REST API and unit tests run on distinct processes / VMs. 
 * The REST API services are executed by a server process, so the DB connections used to perform the changes on the DB are created and committed there.
 * The unit tests, running on other processes, would not have access to those connections in order to do the rollbacks.
 * 
 * Consequently, in order to have the database in the same state before each test, the tests would have to reset the database before each test, 
 * or the data be loaded once for all the tests, having planned which data would be used for which tests in order to avoid test collateral effecting 
 * other tests (harder to maintain). 
 * 
 * @author Davi
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ITMealService {

	private static final String MEAL_RESOURCE = RestPaths.MEALS + "/{id}";	
	
	@Value("${local.server.port}")
	private int serverPort;
	
	@Value("${server.context-path}")
	private String context;
	
	@Autowired
	private TestData testData; 
	
	@Autowired
	// for removing created entries after tests
	private MealRepository mealRepository;
	
	private String accessToken = null;
	
	private Meal mealForCreate;
	
	private static AtomicInteger mealToAlter = new AtomicInteger(1);
	
	@Before
	public void setUp() {
	    RestAssured.port = serverPort;
	    RestAssured.basePath = context;
	    
	    // need to change Jackson object mapper (JSON serializer/deserializer) because of specific date parsing required for time attributes  
	    RestAssured.config = RestAssuredConfig.config().objectMapperConfig(new ObjectMapperConfig().jackson2ObjectMapperFactory(
	    		new Jackson2ObjectMapperFactory() {
	    		        @Override
	    		        public ObjectMapper create(@SuppressWarnings("rawtypes") Class aClass, String s) {
	    		            ObjectMapper mapper = new ObjectMapper();
	    		    		SimpleModule module = new SimpleModule();
	    		    		module.addDeserializer(Date.class, new CustomDateDeserializer());
	    		    		mapper.registerModule(module);

	    		            return mapper;
	    		        }
	    		}
	    ));
	    
	    mealForCreate = Meal.builder().user(testData.user1).mealDate(new Date()).mealTime(new Date()).description("meal desc").calories(1765).build();
	}

	/*******************************************************************************************************************************/
	/***   Authentication for all authenticated tests                                                                            ***/
	/*******************************************************************************************************************************/
	
	// we need to get the oauth token before we can perform the request
	private void authenticateUser(String username, String password) {
		
		String response =
	            given().
	                parameters("username", username, "password", password, "grant_type", "password", "scope", "read write", "client_id", "clientapp", "client_secret", "123456").
	                auth().
	                preemptive().
	                basic("clientapp","123456").
	            when().
	                post("/oauth/token").
	                asString();
		
		JsonPath jsonPath = new JsonPath(response);
		accessToken = jsonPath.getString("access_token");
	}
	
	/*******************************************************************************************************************************/
	/***   Get Meal tests                                                                                                        ***/
	/*******************************************************************************************************************************/
	
	@Test
	public void testGetMealNotAuth() {
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.get(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}	
	
	@Test
	public void testGetMealInexistent() {

		authenticateUser(testData.user1.getLogin(), "1");

		given()
			.auth().oauth2(accessToken)
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.get(MEAL_RESOURCE, -1)
		.then()
			.log().all()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}	

	@Test
	public void testGetMealDefaultUserNotOwner() {

		// user 2 will try to get one of user 1's meal 
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.get(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
		.then()
			.log().all()
			.statusCode(HttpStatus.FORBIDDEN.value());
	}	

	@Test
	public void testGetMealDefaultUserOwner() {
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		Meal meal = 
			given()
				.auth().oauth2(accessToken)
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
			.expect()
				.log().all()
				.statusCode(HttpStatus.OK.value())
			.when()
				.get(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
				.as(Meal.class);
		
		assertThat(meal).isEqualTo(testData.userMeals.get(testData.user1).get(0));
	}	

	@Test
	public void testGetMealAuthManager() {
		
		authenticateUser(testData.userManager.getLogin(), "1");
		
		Meal meal = 
			given()
				.auth().oauth2(accessToken)			
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
			.expect()
				.log().all()
				.statusCode(HttpStatus.OK.value())
			.when()
				.get(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
				.as(Meal.class);
		
		assertThat(meal).isEqualTo(testData.userMeals.get(testData.user1).get(0));		
	}	

	@Test
	public void testGetMealAuthAdmin() {
		
		authenticateUser(testData.userAdmin.getLogin(), "1");
		
		Meal meal = 
				given()
					.auth().oauth2(accessToken)			
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
				.expect()
					.log().all()
					.statusCode(HttpStatus.OK.value())
				.when()
					.get(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
					.as(Meal.class);
			
		assertThat(meal).isEqualTo(testData.userMeals.get(testData.user1).get(0));		
	}	
	
	/*******************************************************************************************************************************/
	/***   Create Meal tests                                                                                                     ***/
	/*******************************************************************************************************************************/
	
	@Test
	public void testCreateMealNotAuth() {
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(mealForCreate)
		.when()
			.log().all()
			.post(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}	
	
	@Test
	public void testCreateMealDefaultUserNotOwner() {

		// user 2 will try to get one of user 1's meal 
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(mealForCreate)
		.when()
			.post(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.FORBIDDEN.value());
	}	

	@Test
	public void testCreateMealNoUser() {

		Meal meal = mealForCreate.toBuilder().user(null).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.post(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("user"));
	}	

	@Test
	public void testCreateMealMissingMealAttribute() {

		Meal meal = mealForCreate.toBuilder().calories(null).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.post(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("calories"));
	}	

	@Test
	public void testCreateMealUniqueKeyViolation() {
		Meal meal = testData.userMeals.get(testData.user1).get(0).toBuilder().id(null).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.post(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("either user ID is invalid or there is already a meal"));
	}	


	// would need a mock to emulate JPA layer returning null on create (404 scenario)

	@Test
	public void testCreateMealOKDefaultUserOwnsMeal() {
		Integer id = null; 
		try {
			authenticateUser(testData.user1.getLogin(), "1");
			
			Meal mealCreated = 
				given()
					.auth().oauth2(accessToken)			
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(mealForCreate)
				.expect()
					.log().all()
					.statusCode(HttpStatus.CREATED.value())
				.when()
					.post(RestPaths.MEALS)
					.as(Meal.class);
			
			assertThat(mealCreated).isNotNull();
			assertThat(mealCreated.getId()).isNotNull();
			id = mealCreated.getId();
			mealCreated.setId(null);
			assertThat(mealCreated).isEqualTo(mealForCreate);
		} finally {		
			if (id != null)
				mealRepository.delete(id);
		}
	}	
	

	@Test
	public void testCreateMealOKManagerUser() {
		Integer id = null; 
		try {
			authenticateUser(testData.userManager.getLogin(), "1");
			
			Meal mealCreated = 
				given()
					.auth().oauth2(accessToken)			
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(mealForCreate)
				.expect()
					.log().all()
					.statusCode(HttpStatus.CREATED.value())
				.when()
					.post(RestPaths.MEALS)
					.as(Meal.class);
			
			assertThat(mealCreated).isNotNull();
			assertThat(mealCreated.getId()).isNotNull();
			id = mealCreated.getId();
			mealCreated.setId(null);
			assertThat(mealCreated).isEqualTo(mealForCreate);
		} finally {		
			if (id != null)
				mealRepository.delete(id);
		}
	}	
	
	@Test
	public void testCreateMealOKAdminUser() {
		Integer id = null; 
		try {
			authenticateUser(testData.userAdmin.getLogin(), "1");
			
			Meal mealCreated = 
				given()
					.auth().oauth2(accessToken)			
					.contentType(ContentType.JSON)
					.accept(ContentType.JSON)
					.body(mealForCreate)
				.expect()
					.log().all()
					.statusCode(HttpStatus.CREATED.value())
				.when()
					.post(RestPaths.MEALS)
					.as(Meal.class);
			
			assertThat(mealCreated).isNotNull();
			assertThat(mealCreated.getId()).isNotNull();
			id = mealCreated.getId();
			mealCreated.setId(null);
			assertThat(mealCreated).isEqualTo(mealForCreate);
		} finally {		
			if (id != null)
				mealRepository.delete(id);
		}
	}	
	
	/*******************************************************************************************************************************/
	/***   Update Meal tests                                                                                                     ***/
	/*******************************************************************************************************************************/
	
	@Test
	public void testUpdateMealNotAuth() {
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			// no need to change the meal since it is expected the request will be denied
			.body(testData.userMeals.get(testData.user1).get(0))
		.when()
			.log().all()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}	
	
	@Test
	public void testUpdateMealDefaultUserNotOwner() {

		// user 2 will try to get one of user 1's meal 
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			// no need to change the meal since it is expected the request will be denied
			.body(testData.userMeals.get(testData.user1).get(0))
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.FORBIDDEN.value());
	}	

	@Test
	public void testUpdateMealNoUser() {

		Meal meal = testData.userMeals.get(testData.user1).get(0).toBuilder().user(null).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("user"));
	}	

	@Test
	public void testUpdateMealMissingMealAttribute() {

		Meal meal = testData.userMeals.get(testData.user1).get(0).toBuilder().description(null).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("description"));
	}	

	@Test
	public void testUpdateMealTamperedUserId() {
		// user 2 tries to modify user1's meal by tampering the user ID in the meal details
		Meal meal = testData.userMeals.get(testData.user1).get(0).toBuilder().user(testData.user2).build();
		
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.FORBIDDEN.value())
			.body("message", Matchers.containsString("No privileges to update meal"));
			//.body("error", Matchers.equalTo("access_denied"));
	}	

	@Test
	public void testUpdateMealNullMealId() {
		// user 2 tries to modify user1's meal by tampering the user ID in the meal details
		Meal meal = testData.userMeals.get(testData.user2).get(0).toBuilder().id(null).build();
		
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("Meal ID should NOT be null"));
			//.body("error", Matchers.equalTo("access_denied"));
	}	

	@Test
	public void testUpdateMealInvalidMealId() {
		// user 2 tries to modify user1's meal by tampering the user ID in the meal details
		Meal meal = testData.userMeals.get(testData.user2).get(0).toBuilder().id(Integer.MAX_VALUE).build();
		
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.NOT_FOUND.value())
			.body("message", Matchers.containsString("not found"));
			//.body("error", Matchers.equalTo("access_denied"));
	}	
	
	
	@Test
	public void testUpdateMealUniqueKeyViolation() {
		// take the last meal to make sure it was not modified by other tests
		List<Meal> userMeals = testData.userMeals.get(testData.user1); 
		Meal otherMeal = userMeals.get(userMeals.size()-1);
		Meal meal = testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).toBuilder().mealDate(otherMeal.getMealDate()).mealTime(otherMeal.getMealTime()).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(meal)
		.when()
			.put(RestPaths.MEALS)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("either user ID is invalid or there is already another meal"));
	}	

	// would need a mock to emulate JPA layer returning null on update (404 scenario)

	@Test
	public void testUpdateMealOKDefaultUserOwnsMeal() {
		Meal meal = testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).toBuilder().description("CHANGED BY Integration Test 1").mealTime(new Date()).build();
		
		authenticateUser(testData.user1.getLogin(), "1");
		
		Meal mealUpdated = 
			given()
				.auth().oauth2(accessToken)			
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.body(meal)
			.expect()
				.log().all()
				.statusCode(HttpStatus.OK.value())
			.when()
				.put(RestPaths.MEALS)
				.as(Meal.class);
			
		assertThat(mealUpdated).isNotNull();
		assertThat(mealUpdated).isEqualTo(meal);
	}	
	

	@Test
	public void testUpdateMealOKManagerUser() {
		// careful with changing date or time... there are several other meals on the same time on distinct days for the same user
		Meal meal = testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).toBuilder().mealDate(new Date(System.currentTimeMillis()-30*24*60*60*1000)).calories(2432).build();
		
		authenticateUser(testData.userManager.getLogin(), "1");
		
		Meal mealUpdated = 
			given()
				.auth().oauth2(accessToken)			
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.body(meal)
			.expect()
				.log().all()
				.statusCode(HttpStatus.OK.value())
			.when()
				.put(RestPaths.MEALS)
				.as(Meal.class);
			
		assertThat(mealUpdated).isNotNull();
		assertThat(mealUpdated).isEqualTo(meal);
	}	
	
	@Test
	public void testUpdateMealOKAdminUser() {
		Meal meal = testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).toBuilder().description("CHANGED BY Integration Test 3").build();
		
		authenticateUser(testData.userAdmin.getLogin(), "1");
		
		Meal mealUpdated = 
			given()
				.auth().oauth2(accessToken)			
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.body(meal)
			.expect()
				.log().all()
				.statusCode(HttpStatus.OK.value())
			.when()
				.put(RestPaths.MEALS)
				.as(Meal.class);
			
		assertThat(mealUpdated).isNotNull();
		assertThat(mealUpdated).isEqualTo(meal);
	}	
	
	/*******************************************************************************************************************************/
	/***   Delete Meal tests                                                                                                     ***/
	/*******************************************************************************************************************************/
	
	@Test
	public void testDeleteMealNotAuth() {
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.log().all()
			.delete(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value());
	}	

	@Test
	public void testDeleteMealDefaultUserNotOwner() {

		// user 2 will try to get one of user 1's meal 
		authenticateUser(testData.user2.getLogin(), "2");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.delete(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(0).getId())
		.then()
			.log().all()
			.statusCode(HttpStatus.FORBIDDEN.value());
	}	
	
	// if not authenticated will return 401, due to spring security configuration in security configuration class
	@Test
	public void testDeleteMealNotFound() {
		authenticateUser(testData.user1.getLogin(), "1");

		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.when()
			.log().all()
			.delete(MEAL_RESOURCE, -1)
		.then()
			.log().all()
			.statusCode(HttpStatus.NOT_FOUND.value());
	}	

	// would need a mock to emulate JPA layer returning not found exception on delete (404 scenario)

	@Test
	public void testDeleteMealOKDefaultUserOwnsMeal() {
		authenticateUser(testData.user1.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.expect()
			.log().all()
			.statusCode(HttpStatus.NO_CONTENT.value())
		.when()
			.delete(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).getId());
	}	
	

	@Test
	public void testDeleteMealOKManagerUser() {
		authenticateUser(testData.userManager.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.expect()
			.log().all()
			.statusCode(HttpStatus.NO_CONTENT.value())
		.when()
			.delete(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).getId());
			
	}	
	
	@Test
	public void testDeleteMealOKAdminUser() {
		authenticateUser(testData.userAdmin.getLogin(), "1");
		
		given()
			.auth().oauth2(accessToken)			
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
		.expect()
			.log().all()
			.statusCode(HttpStatus.NO_CONTENT.value())
		.when()
			.delete(MEAL_RESOURCE, testData.userMeals.get(testData.user1).get(mealToAlter.getAndIncrement()).getId());
		
	}	
	
}
