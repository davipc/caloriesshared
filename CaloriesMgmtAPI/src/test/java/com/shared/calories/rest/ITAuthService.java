package com.shared.calories.rest;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.shared.calories.Application;
import com.shared.calories.constants.RestPaths;
import com.shared.calories.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)

//USE OF DBUNIT IS CONFLICTING WITH TEST DATA (NEED TO REPLACE TEST DATA)
//@DatabaseSetup(ITAuthService.DATASET)
//@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { ITAuthService.DATASET })
//@DirtiesContext
//@TestExecutionListeners({ 
//		DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
//		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class 
//})
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ITAuthService {

	protected static final String DATASET = "classpath:datasets/all-entries.xml";
	
	@Value("${local.server.port}")
	private int serverPort;
	
	@Value("${server.context-path}")
	private String context;
	
	@Before
	public void setUp() {
	    RestAssured.port = serverPort;
	    RestAssured.basePath = context;
	}

	
	@Test
	public void testAuthenticateBadMethodGet() 
	throws JsonProcessingException {
		User user = User.builder().login(null).password("password").build(); 
		
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(user)
		.when()
			.get(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.METHOD_NOT_ALLOWED.value());
	}	
	
	@Test
	public void testAuthenticateBadMediaType() 
	throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		User user = User.builder().login("user1").password("password").build(); 
		String json = mapper.writeValueAsString(user);
		
		given()
			.contentType(ContentType.URLENC)
			.accept(ContentType.JSON)
			.body(json)
		.when()
			.post(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value());
	}	
	
	@Test
	public void testAuthenticateUserMissingLogin() 
	throws JsonProcessingException {
		User user = User.builder().login(null).password("password").build(); 
		
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(user)
		.when()
			.post(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("Missing login"));
	}	
	
	@Test
	public void testAuthenticateUserMissingPassword() 
	throws JsonProcessingException {
		User user = User.builder().login("dcavalca").password(null).build(); 
		
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			.body(user)
		.when()
			.post(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.BAD_REQUEST.value())
			.body("message", Matchers.containsString("Missing"));
	}	
	
	
	@Test
	public void testAuthenticateUserBadLogin() 
	throws JsonProcessingException {
		User user = User.builder().login("XXXXX").password("password").build(); 
		
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			// need to use .getJSON since password will not be in Jackson serialized JSON 
			.body(user.getJSON())
		.when()
			.post(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value())
			.body("message", Matchers.is("Invalid login/password"));
	}	

	@Test
	public void testAuthenticateUserBadPassword() 
	throws JsonProcessingException {
		User user = User.builder().login("user1").password("2").build(); 
		
		given()
			.contentType(ContentType.JSON)
			.accept(ContentType.JSON)
			// need to use .getJSON since password will not be in Jackson serialized JSON 
			.body(user.getJSON())
		.when()
			.post(RestPaths.AUTH)
		.then()
			.log().all()
			.statusCode(HttpStatus.UNAUTHORIZED.value())
			.body("message", Matchers.is("Invalid login/password"));
	}	

	@Test
	public void testAuthenticateUserGoodCredentials() 
	throws JsonProcessingException {
		User user = User.builder().login("user1").password("1").build(); 
		String userJSON = user.getJSON();
		
		User returnedUser = 
			given()
				.contentType(ContentType.JSON)
				.accept(ContentType.JSON)
				.body(userJSON)
				.log().all()
			.expect()
				.statusCode(HttpStatus.OK.value())
				.log().all()
			.when()
				.post(RestPaths.AUTH)
				.as(User.class);
		
		assertThat(returnedUser).isNotNull();
		assertThat(returnedUser.getLogin()).isEqualTo("user1");
	}	
	
}
