package com.shared.calories.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.shared.calories.Application;
import com.shared.calories.entity.Role;
import com.shared.calories.entity.RoleType;
import com.shared.calories.repository.RoleRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)

// USE OF DBUNIT IS CONFLICTING WITH TEST DATA (NEED TO REPLACE TEST DATA)
//@DatabaseSetup(TestRoleRepository.DATASET)
//@DatabaseTearDown(type = DatabaseOperation.DELETE_ALL, value = { TestRoleRepository.DATASET })
//@DirtiesContext
//@TestExecutionListeners({ 
//		DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class,
//		TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class 
//})

public class TestRoleRepository {

//	protected static final String DATASET = "classpath:datasets/all-entries.xml";

	@Autowired
	protected RoleRepository repository;
	
	
	@Test
	public void testRead() {
		Role expected = Role.builder().name(RoleType.DEFAULT).build();
		
		Role read = repository.findOne(4);

		log.info("Role found: " + read);

		assertThat(read).isEqualToIgnoringGivenFields(expected, "id");
	}
	
	@Test
	public void testFindAll() {
		List<Role> allRoles = (List<Role>) repository.findAll();
		assertThat(allRoles).hasSize(3);
		for (Role role: allRoles) {
			log.debug("Found role: " + role);
			assertThat(role.getId()).isIn(new Object[]{4,5,6});
		}
		
	}
	
	@Test
	public void testFindByName() {
		Role role = repository.findByName(RoleType.ADMIN);
		assertThat(role).isNotNull();
		assertThat(role.getId()).isEqualTo(6);
	}
}
