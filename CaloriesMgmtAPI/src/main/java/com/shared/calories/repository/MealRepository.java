package com.shared.calories.repository;

import org.springframework.data.repository.CrudRepository;

import com.shared.calories.entity.Meal;

public interface MealRepository extends CrudRepository<Meal, Integer> {
}
