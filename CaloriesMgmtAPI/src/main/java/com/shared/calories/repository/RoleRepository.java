package com.shared.calories.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.shared.calories.entity.Role;
import com.shared.calories.entity.RoleType;

public interface RoleRepository extends CrudRepository<Role, Integer> {
	
	public Role findByName(@Param("name") RoleType name); 
}
