package com.shared.calories.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shared.calories.entity.Role;
import com.shared.calories.repository.RoleRepository;
import com.shared.calories.rest.exceptions.NotFoundException;
import com.shared.calories.rest.util.RestUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/api/v2/roles")
public class RoleService extends ExceptionAwareService {

	@Autowired
	RoleRepository repository; 

	@RequestMapping(method=RequestMethod.GET)
	public @ResponseBody List<Role> getRoles() 
	throws NotFoundException {
		log.debug("Fetching all roles"); 
		
		List<Role> roles = RestUtil.makeList(repository.findAll());
		
		log.debug("Roles found: " + roles.size());
		return roles;
	}
}
