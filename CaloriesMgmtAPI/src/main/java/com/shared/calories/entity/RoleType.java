package com.shared.calories.entity;

public enum RoleType {
	DEFAULT,
	MANAGER,
	ADMIN;
}
