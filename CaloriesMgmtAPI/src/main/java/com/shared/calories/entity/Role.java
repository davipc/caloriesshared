package com.shared.calories.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the role database table.
 * 
 */

@JsonPropertyOrder({"id", "name"})

@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder(toBuilder = true)

@Entity
public class Role extends BaseEntity implements GrantedAuthority {

	private static final long serialVersionUID = 1582621316792956215L;

	@Id
	@SequenceGenerator(name="role_id_seq", sequenceName="role_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_id_seq")
	private Integer id;

	// this field can never be modified by other entities updates
	@Column(nullable=false, length=20, updatable=false)
	@Enumerated(EnumType.STRING)
	private RoleType name;

	/** not really needed
	//bi-directional many-to-many association to User
	@ManyToMany
	@JoinTable(
		name="user_role"
		, joinColumns={
			@JoinColumn(name="role_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="user_id")
			}
		)
	private List<User> users;
	**/
	
	@JsonIgnore
	@Override
	public String getAuthority() {
		return "ROLE_" + name.name();
//		return name.name();
	}	
	
	@JsonIgnore
	public String getJSON() {
		StringBuilder builder = new StringBuilder("{");
		builder.append("\"id\":").append(id).append(",");
		builder.append("\"name\":").append(quoteObject(name));
		builder.append("}");
		
		return builder.toString();
	}
}