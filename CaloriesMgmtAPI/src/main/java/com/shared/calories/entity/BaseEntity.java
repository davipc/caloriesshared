package com.shared.calories.entity;

public abstract class BaseEntity {
	
	/**
	 * Validates the input String, returning the errors in case it is not valid
	 * @param attr The attribute to validate
	 * @param required True if the attribute is required (not null)
	 * @param maxSize The string max size
	 * @param attrName The attribute name (for reporting purposes)
	 * @return An empty String if there are no errors, or the errors if there are any. 
	 */
	public String validateString(String attr, boolean required, int maxSize, String attrName) {
		StringBuilder sb = new StringBuilder();
		
		if (attr == null) {
			if (required) { 
				sb.append(attrName + " is null; ");
			}
		} else if (attr.length() > maxSize) {
			sb.append(attrName + " size is bigger than " + maxSize + "; ");
		}
		
		return sb.toString();
	}
	

	/**
	 * Validates the input Object, returning the errors in case it is not valid
	 * @param attr The attribute to validate
	 * @param required True if the attribute is required (not null)
	 * @param attrName The attribute name (for reporting purposes)
	 * @return An empty String if there are no errors, or the errors if there are any. 
	 */
	public String validateForNull(Object attr, boolean required, String attrName) {
		StringBuilder sb = new StringBuilder();
		
		if (attr == null && required) { 
			sb.append(attrName + " is null; ");
		}
		
		return sb.toString();
	}
	
	public String quoteObject(Object obj) {
		String result = null;
		if (obj != null) {
			result = "\"" + obj + "\"";
		}
		
		return result;
	}
}
