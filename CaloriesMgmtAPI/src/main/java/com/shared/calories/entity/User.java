package com.shared.calories.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;


/**
 * The persistent class for the app_user database table.
 * 
 */

@JsonPropertyOrder({"id", "login", "password", "name", "gender", "dailyCalories", "roles", "creationDt"})

@Data
// need to exclude password since it is not returned from JSON serializations (REST API reads, for instance)
@EqualsAndHashCode(exclude="password", callSuper=false)
@Getter
@AllArgsConstructor
@NoArgsConstructor
// need to exclude meals list to break cycle
@ToString(exclude="meals")
@Builder(toBuilder = true)

@Entity
@Table(name="app_user",
		uniqueConstraints={@UniqueConstraint(name = "User_UNIQ", columnNames = {"login"})},
		indexes = {@Index(name="User_IDX", columnList="login")
})
@NamedQueries ({
	@NamedQuery(name="User.findMealsInDateAndTimeRange", 
			query="SELECT m FROM User u JOIN u.meals m where m.user.id = :userId and m.mealDate between :fromDate and :toDate and m.mealTime between :fromTime and :toTime")
	//@NamedQuery(name="User.findByLogin", query="SELECT u FROM User u where u.login = :login")

})

public class User extends BaseEntity {

	@Id
	@SequenceGenerator(name="user_id_seq", sequenceName="user_id_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="user_id_seq")
	@Column(name = "id", updatable=false, nullable=false)	
	private Integer id;

	@Column(nullable=false, unique=true, length=12)
	private String login;

	// makes sure this is not present in the generated JSON, but read when creating a user from a JSON
	@JsonProperty(access=Access.WRITE_ONLY)
	@Column(nullable=false, length=64)
	private String password;

	@Column(nullable=false, length=80)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(nullable=false, length=1)
	private Gender gender;

	@Column(name="daily_calories", nullable=false)
	private Integer dailyCalories;

	//one-directional many-to-many association to Role
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="user_role" , 
		foreignKey=@ForeignKey(name="user_id_FK"),
		joinColumns={@JoinColumn(name="user_id", referencedColumnName = "id")}, 
		inverseForeignKey=@ForeignKey(name="role_id_FK"),
		inverseJoinColumns={@JoinColumn(name="role_id", referencedColumnName = "id")}
	)
	private List<Role> roles;

	// makes sure this is not present in the generated JSON
	@JsonIgnore
	@XmlTransient
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL, mappedBy="user")
	private List<Meal> meals;
	
	@Column(name="creation_dt", nullable=false)
	private Timestamp creationDt;

	
	public User(User otherUser) {
		this.id = otherUser.id;
		this.login = otherUser.login;
		this.password = otherUser.password;
		this.name = otherUser.name;
		this.gender = otherUser.gender;
		this.dailyCalories = otherUser.dailyCalories;
		
		if (otherUser.roles != null) {
			this.roles = new ArrayList<Role>(otherUser.roles);
		}
//		if (otherUser.meals != null && otherUser.meals.size() > 0) {
//			this.meals = new ArrayList<Meal>(otherUser.meals);
//		}
	}

	public boolean hasRole(RoleType roleToCheck) {
		boolean result = false;
		
		for (Role role: roles) {
			if (role.getName().equals(roleToCheck)) {
				result = true;
				break;
			}
		}
			
		return result;
	}
	
	@JsonIgnore
	public String getJSON() {
		StringBuilder builder = new StringBuilder("{");
		builder.append("\"id\":").append(id).append(",");
		builder.append("\"login\":").append(quoteObject(login)).append(",");
		builder.append("\"password\":").append(quoteObject(password)).append(",");
		builder.append("\"name\":").append(quoteObject(name)).append(",");
		builder.append("\"gender\":").append(quoteObject(gender)).append(",");
		builder.append("\"dailyCalories\":").append(dailyCalories).append(",");
		builder.append("\"roles\":");
		
		if (roles == null) {
			builder.append("null, ");
		} else {
			builder.append("[");
			if (roles.size() > 0) {
				builder.append(roles.get(0).getJSON());
				for (int i = 1; i < roles.size(); i++) {
					builder.append(", ").append(roles.get(i).getJSON());
				}
			}
			builder.append("], ");
		}
		builder.append("\"creationDt\":").append(creationDt != null ? creationDt.getTime() : null).append("}");
		
		return builder.toString();
	}
	
	
	/**
	 * Validates the object, returning a list of errors if any exist, or null otherwise
	 * @return
	 */
	public String validate() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(validateString(login, true, 12, "login"));
		// no need to worry about password field size - hash function will always return 64 chars
		sb.append(validateString(password, false, Integer.MAX_VALUE, "password"));
		sb.append(validateString(name, true, 80, "name"));
		sb.append(validateForNull(gender, true, "gender"));
		sb.append(validateForNull(dailyCalories, true, "daily calories"));
		sb.append(validateForNull(creationDt, true, "creation date"));
		
		String result = sb.toString();
		if (result.trim().equals(""))
			result = null;
		
		return result;
	}
}