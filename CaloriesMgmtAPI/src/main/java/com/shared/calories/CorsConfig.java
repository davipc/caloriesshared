package com.shared.calories;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.shared.calories.constants.RestPaths;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class CorsConfig {
	
	@Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            /**
             * Need this to enable cross domain calls.
             * Also need to configure the resource server to permit all OPTIONS requests, otherwise CORS OPTIONS requests will be denied by OAuth2 and thus CORS requests will fail.  
             */
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                log.debug("Configuring CORS...");
            	registry.addMapping(RestPaths.REST_BASE_URI + "/**")
                		.allowedMethods(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod.DELETE.name()
                				//, HttpMethod.HEAD.name(), HttpMethod.OPTIONS.name()
                				);
            }
        };
    }	
}
