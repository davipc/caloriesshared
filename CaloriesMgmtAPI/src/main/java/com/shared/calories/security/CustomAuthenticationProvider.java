package com.shared.calories.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import com.shared.calories.entity.User;
import com.shared.calories.rest.AuthService;
import com.shared.calories.rest.exceptions.NotFoundException;
import com.shared.calories.rest.exceptions.UnauthorizedException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private AuthService authSvc;
	
    @Override
    public Authentication authenticate(Authentication authentication)
	throws AuthenticationException {
        Authentication result = null; 
    	String name = authentication.getName();

        log.debug("Authenticating user " + name);
        // You can get the password here
        String password = authentication.getCredentials().toString();

        // call authentication method
        User authenticatedUser = callCustomAuthenticationMethod(name, password);

        // if authenticated (and thus returned a user), create the return object with the credentials + roles  
        if (authenticatedUser != null) {
        	
        	List<? extends GrantedAuthority> authorities = authenticatedUser.getRoles();
        	
        	result = new UsernamePasswordAuthenticationToken(authenticatedUser, password, authorities);
        }

        log.debug("Finished authenticating user " + name);
        
        return result;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    /**
     * Calls the authentication REST API
     * @param username
     * @param password
     * @return
     */
    private User callCustomAuthenticationMethod(String username, String password) {
    	User user = null;
    	
    	User loginUser = new User();
    	loginUser.setLogin(username);
    	loginUser.setPassword(password);
    	
    	try {
	    	user = authSvc.authenticateUser(loginUser);
    	} catch (UnauthorizedException | NotFoundException e) {
    		log.warn("Error authenticating user " + username + ": " + e.getMessage());
    	}
    	return user;
    }
}