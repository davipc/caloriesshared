package com.shared.calories.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.shared.calories.rest.util.EncryptionHelper;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

//	@Autowired
//    private CustomAuthenticationProvider authProvider;

	
	@Autowired
	private CustomUserDetailsService userDetailsService;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		// uncomment this and injected user details service and encoder to perform authentication by using repository and checking hashed password
		auth
		 	.userDetailsService(userDetailsService)
		 	.passwordEncoder(passwordEncoder);
		
        // uncomment this and injected provider to call authentication externally 
//		auth.authenticationProvider(authProvider);
		
	}

	@Override
	@Bean
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        final EncryptionHelper encHelper = new EncryptionHelper(); 
		
		return new PasswordEncoder() {
			
			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				String encoded = encode(rawPassword);
				return encoded.equals(encodedPassword);
			}
			
			@Override
			public String encode(CharSequence rawPassword) {
				return encHelper.encrypt(rawPassword.toString());
			}
		}; 
    }	
}
