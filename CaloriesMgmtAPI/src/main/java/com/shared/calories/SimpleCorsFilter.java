package com.shared.calories;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This filter is needed when oauth2 is used in conjunction with CORS (cross domain calls - one deployed application performing Ajax calls to another in our case)
 * 
 * Without this filter, the CORS generated OPTIONS requests that come before the Ajax PUT/POST/DELETE calls will fail with 401 if the target endpoint is protected 
 * in OAuth (requires authentication).
 *  
 * Other options are to: 
 * 	- configure the CORS mappings in an adapter (in CorsConfig in our case) and OAUth2 Resource Configuration to permit all OPTIONS requests (in OAuth2ServerConfiguration)
 *  - configure a CorsFilter instead of a simple filter (not tested)   
 *  
 * @author Davi
 *
 */

//@Component
//@Order(Ordered.HIGHEST_PRECEDENCE)
public class SimpleCorsFilter implements Filter {

	public SimpleCorsFilter() {
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
//		HttpServletResponse response = (HttpServletResponse) res;
//		HttpServletRequest request = (HttpServletRequest) req;
//		
//		response.setHeader("Access-Control-Allow-Origin", "*");
//		response.setHeader("Access-Control-Allow-Methods", "POST,PUT, GET, OPTIONS, DELETE");
//		response.setHeader("Access-Control-Max-Age", "3600");
//		response.setHeader("Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
//
//		if (HttpMethod.OPTIONS.name().equalsIgnoreCase(request.getMethod())) {
//			response.setStatus(HttpServletResponse.SC_OK);
//		} else {
			chain.doFilter(req, res);
//		}
	}

	@Override
	public void init(FilterConfig filterConfig) {
	}

	@Override
	public void destroy() {
	}

}