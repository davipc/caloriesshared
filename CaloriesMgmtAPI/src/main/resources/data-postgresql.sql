insert into ROLE values (nextval('role_id_seq'), 'DEFAULT');
insert into ROLE values (nextval('role_id_seq'), 'MANAGER');
insert into ROLE values (nextval('role_id_seq'), 'ADMIN');
  
insert into APP_USER (id, name, login, password, gender, daily_calories, creation_dt) values (nextval('user_id_seq'), 'User 1', 'user1', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'M', 1500, '2016-03-15 16:45:15');  
insert into APP_USER (id, name, login, password, gender, daily_calories, creation_dt) values (nextval('user_id_seq'), 'User 2', 'user2', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'F', 1600, '2016-03-16 17:46:16');  
insert into APP_USER (id, name, login, password, gender, daily_calories, creation_dt) values (nextval('user_id_seq'), 'User 3', 'user3', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'M', 1700, '2016-03-17 18:47:17');  
insert into APP_USER (id, name, login, password, gender, daily_calories, creation_dt) values (nextval('user_id_seq'), 'Manager 1', 'manager1', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'F', 1800, '2016-03-18 19:48:18');  
insert into APP_USER (id, name, login, password, gender, daily_calories, creation_dt) values (nextval('user_id_seq'), 'Admin 1', 'admin1', 'XohImNooBHFR0OVvjcYpJ3NgPQ1qq73WKhHvch0VQtg=', 'M', 1900, '2016-03-19 20:49:19');  
  
insert into USER_ROLE values (1, 1);
insert into USER_ROLE values (2, 1);
insert into USER_ROLE values (3, 1);
insert into USER_ROLE values (4, 2);
insert into USER_ROLE values (5, 2);
insert into USER_ROLE values (5, 3);

insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 1, now(), now(), 'meal desc 1.1', 1500);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 2, now(), now() - interval '1 hour', 'meal desc 2.1', 1400);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 2, now(), now(), 'meal desc 2.2', 1600);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 4, now(), now(), 'meal desc 4.1', 1300);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 5, now(), now() - interval '2 hours', 'meal desc 5.1', 1700);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 5, now(), now() - interval '1 hour', 'meal desc 5.2', 1800);
insert into MEAL (id, user_id, meal_date, meal_time, description, calories) values (nextval('meal_id_seq'), 5, now(), now(), 'meal desc 5.3', 1900);