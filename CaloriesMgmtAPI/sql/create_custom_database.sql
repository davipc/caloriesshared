-- RAN AND TESTED PostgreSQL 9.5 @ ON WINDOWS 7 

psql -d postgres -U postgres -c "create user shared_calories_spring with password 'password';"
psql -d postgres -U shared_calories_spring -c "select 'test';"
psql -d postgres -U postgres -c "create database shared_calories_spring template postgres;"
psql -d postgres -U postgres -c "grant all privileges on database shared_calories_spring to shared_calories_spring;"

psql -U shared_calories_spring -d shared_calories_spring

CREATE TABLE test(ID VARCHAR(32), TEST_MSG VARCHAR(64));
insert into test values ('1','SOME CRAZY MESSAGE');
select * from test;
drop table test;
\q

